// Gulp configuration

// include gulp and plugins
var 
    gulp = require('gulp'),
    newer = require('gulp-newer'), // compare and adds only on new files
    concat = require('gulp-concat'),  // concat files
    preprocess = require('gulp-preprocess'), //html preprocess for templating
    htmlclean = require('gulp-htmlclean'),
    imacss = require('gulp-imacss'),
    pleeease = require('gulp-pleeease'),    
    sass = require('gulp-sass'),
    deporder = require('gulp-deporder'), //order js dependencies
    uglify = require('gulp-uglify'), // minify javascript 
    sourcemaps = require('gulp-sourcemaps'),
    gutil = require('gulp-util'),
    size = require('gulp-size'), // show sizes for optimization values
    del = require('del'),
    pkg = require('./package.json'),
    browsersync = require('browser-sync');

    // for image compression and optimization
    // https://gist.github.com/LoyEgor/e9dba0725b3ddbb8d1a68c91ca5452b5 
    var cache = require('gulp-cache');
    var imagemin = require('gulp-imagemin');
    var imageminPngquant = require('imagemin-pngquant');
    var imageminZopfli = require('imagemin-zopfli');
    var imageminMozjpeg = require('imagemin-mozjpeg'); //need to run 'brew install libpng'
    var imageminGiflossy = require('imagemin-giflossy');

// file locations
var 
    devBuild = true,

    source = 'source/',
    dest = 'build/',

    html = {
        in: source + '*.html', 
        watch: [source + '*.html', source + 'template/**/*'], // Watch html files and every html file and subfolder insdide template
        out: dest,
        context: { // Variables used on template files
            devBuild: devBuild,
            author: pkg.author,
            version: pkg.version
        }
    },
    
    images = {
        in: source + 'images/*.*',
        out: dest + 'images/'
    },

    imguri = {
        in: source + "images/inline/*",
        out: source + 'scss/',
        filename: '_datauri.scss',
        namespace: 'img'
    },
    
    css = {
        in: [
            'node_modules/bootstrap/dist/css/bootstrap.css',
            //'node_modules/owl.carousel/src/scss/owl.carousel.scss', 
            'node_modules/owl.carousel/dist/assets/owl.carousel.css',
            source + 'scss/main.scss'
        ],
        watch: [source + 'scss/**/*', '!' + imguri.out + imguri.filename],
        out: dest + 'css/',
        filename: 'main.css',
        sassOpts: { // Sass options when compile
            outputStyle: (devBuild) ? 'nested' : 'compressed',
            imagePath: '../images',
            precision: 3,
            errLogToConsole: true,
            /*includePaths: [
                'node_modules/owl.carousel/src/scss'
            ],
            sourceMap: true*/
        },
        plOpts: { // pleeease options
            autoprefixer: {browsers: ['last 2 versions', '>2%']},
            rem: ['16px'], // base font size for 1rem
            pseudoelements: true, // force double collons  into single
            mqpack: true, // multiple media queries for the same dimension into one
            minifier: !devBuild // Minify css, removes comments and whitespaces only on production build
        }
    },
    
    fonts = {
        in: source + 'fonts/*.*',
        out: dest + 'fonts/'
    },
    
    js = {
        in: [
            'node_modules/jquery/dist/jquery.js', 
            'node_modules/popper.js/dist/umd/popper.js', 
            'node_modules/bootstrap/dist/js/bootstrap.js',
            'node_modules/owl.carousel/dist/owl.carousel.min.js', 
            source + 'js/**/*'],
        out: dest + 'js',
        filename: 'main.js'
    },
    
    syncOptions = {
        server: {
            baseDir: dest,
            index: 'index.html'
        },
        open: false,
        notify: true
    }; 

// show build type
console.log("Project Name: "+pkg.name+" "+pkg.version+", "+(devBuild ? 'Development' : 'Production')+" build");

// clean the build folder
gulp.task('clean', function(){
    del([
        dest + "*"
    ]);
});

//build HTML files
gulp.task('html', function(){
    var page = gulp.src(html.in).pipe(preprocess({ context: html.context }));

    if(!devBuild){ // if is production build then clean and minify html
        page = page
        .pipe(size({title: 'HTML in: '}))
        .pipe(htmlclean())
        .pipe(size({title: 'HTML out: '}));
    }

    return page.pipe(gulp.dest(html.out));
});

// manage images - mejor usar el task que sigue (imgcompress)
gulp.task('images', function () {
    return gulp.src(images.in)
        .pipe(newer(images.out))
        .pipe(imagemin())
        .pipe(gulp.dest(images.out));
});

//compress all images
gulp.task('imgcompress', function () {
    return gulp.src([images.in])
        .pipe(newer(images.out))
        .pipe(size({title: 'Images in: '}))
        .pipe(cache(imagemin([
            //png
            imageminPngquant({
                speed: 1,
                quality: 98 //lossy settings
            }),
            imageminZopfli({
                more: true
                // iterations: 50 // very slow but more effective
            }),
            imageminGiflossy({
                optimizationLevel: 3,
                optimize: 3, //keep-empty: Preserve empty transparent frames
                lossy: 2
            }),
            //svg
            imagemin.svgo({
                plugins: [{
                    removeViewBox: false
                }]
            }),
            //jpg lossless
            imagemin.jpegtran({
                progressive: true
            }),
            //jpg very light lossy, use vs jpegtran
            imageminMozjpeg({
                quality: 90
            })
        ])))
        .pipe(size({ title: 'Images out: ' }))
        .pipe(gulp.dest(images.out));
});

// convert inline images to dataURIs in SCSS source
gulp.task('imguri', function(){
    return gulp.src(imguri.in)
    .pipe(imagemin())
    .pipe(imacss(imguri.filename, imguri.namespace))
    .pipe(gulp.dest(imguri.out));
});

// copy fonts
gulp.task('fonts', function(){
    return gulp.src(fonts.in)
    .pipe(newer(fonts.out))
    .pipe(gulp.dest(fonts.out));
});

// compile Sass 
gulp.task('sass', ['imguri'], function(){
    return gulp.src(css.in)
    .pipe(concat(css.filename))
    .pipe(sass(css.sassOpts))
    .pipe(size({title: 'CSS in: '}))
    .pipe(pleeease(css.plOpts))
    .pipe(size({ title: 'CSS out: ' }))
    .pipe(gulp.dest(css.out))
    .pipe(browsersync.reload({stream: true}));
});

// javascript files
gulp.task('js', function(cb){
    if(devBuild){
        return gulp.src(js.in)
        .pipe(newer(js.out))
        .pipe(gulp.dest(js.out));
    } else  {
        del([
            dest + "js/*"
        ]);

        return gulp.src(js.in)
        .pipe(deporder())
        .pipe(concat(js.filename))
        .pipe(sourcemaps.init({ loadMaps: true }))    
        .pipe(uglify()).on('error', gutil.log)
        .pipe(gulp.dest(js.out));
    }
});

// Browsersync
gulp.task('browsersync', function(){
    browsersync(syncOptions);
});

// Watch files
gulp.task('watch', function () {
    // html changes 
    gulp.watch(html.watch, ['html', browsersync.reload]);

    // images changes
    gulp.watch(images.in, ['imgcompress']);

    // fonts changes
    gulp.watch(fonts.in, ['fonts']);

    // sass changes 
    gulp.watch([css.watch, imguri.in], ['sass']);

    // js changes
    gulp.watch(js.in, ['js', browsersync.reload]);
});

// default task
gulp.task('default', ['html', 'imgcompress', 'fonts', 'sass', 'js', 'browsersync', 'watch']);